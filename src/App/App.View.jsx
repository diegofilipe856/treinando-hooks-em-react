import "./App.css";
import Header from "./Header";

export default function AppView({ count, setCount }) {
  return (
    <>
      <h1>Vite + React</h1>
      <Header />
      <div className="card">
        <p>Contador = {count}</p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  );
}
