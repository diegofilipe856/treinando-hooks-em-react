import AppView from "./App.View";
import { useState } from "react";

export default function AppContainer() {
  const [count, setCount] = useState(0);
  return <AppView count={count} setCount={setCount} />;
}
